<!DOCTYPE html>
<html lang="en">
<head>
 <meta charset="UTF-8"/>
 <meta name="viewport" content="width=device-width, initial-scale=1.0">
 <title>SanberBook - Sing Up</title>
</head>
<body>
  <!-- Bagian Header web -->
 <h2>Buat Account Baru!</h2>

 <!-- Form singup -->
 <form method="POST" action="/Welcome"> <!--Biodara Diri-->
  @csrf
  <label for="firstname"> First Name: <br></label>
  <input type="text" id="firstname" name="Fname" placeholder="Nama awal Anda">
  <br>
  <label for="lastname"> Last Name:<br> </label>
  <input type="text" id="lastname" name="Lname" placeholder="Nama akhir Anda">
  <br><br>
  <label>Gender:<br></label>
  <input type="radio" name="Gender" value="M"> Male<br>
  <input type="radio" name="Gender" value="F"> Female<br>
  <input type="radio" name="Gender" value="X"> No Prefer<br><br>
  <label>Nationality<br></label>
  <select name="Nationally">
   <option value="Indonesia">Indonesia</option>
   <option value="Singapura">Singapura</option>
   <option value="Other">Other</option>
  </select>
  <br><br>
  <label>Language Spoken:<br></label>
  <input type="checkbox" name="language" value="Indonesia"> Indonesia<br>
  <input type="checkbox" name="language" value="English"> English<br>
  <input type="checkbox" name="language" value="Other"> Other<br>
  <br>
  <label>Bio:<br></label>
  <textarea cols="25" rows="5" wrap="soft" ></textarea><br>
  <button >Sing Up</button>
 </form>

  <br>
  <p> &#169; Yoga Pratama Sakti 2021 </p>
</body>
</html>