@extends('AdminLTE.Master')
@section('Content')

<div>
        <h2>Edit Post {{$pertanyaan->id}}</h2>
        <form action="/Pertanyaan/{{$pertanyaan->id}}" method="POST">
            @csrf
            @method('PUT')
            <div class="form-group">
                <label for="JudulP">Title</label>
                <input type="text" class="form-control" name="JudulP" value="{{$pertanyaan->Judul}}" id="Judul" placeholder="Masukan Judul">
                @error('JudulP')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="IsiP">body</label>
                <input type="text" class="form-control" name="IsiP"  value="{{$pertanyaan->Isi}}"  id="IsiP" placeholder="Masukkan Body">
                @error('IsiP')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Edit</button>
        </form>
    </div>

    @endsection