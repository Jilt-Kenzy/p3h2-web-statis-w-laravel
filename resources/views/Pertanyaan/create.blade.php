@extends('AdminLTE.Master')

@section('Content')
<div class="ml-3 mr-3">
<div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Form Pertanyaan</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="/Pertanyaan" method="POST">
              @csrf
                <div class="card-body">
                  <div class="form-group">
                    <label for="JudulP">Judul Pertanyaan</label>
                    <input type="text" class="form-control" id="Judul" name="JudulP" placeholder="Judul Pertanyaan">
                    @error('JudulP')
                   <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="IsiP">Pertanyaan</label>
                    <input type="text" class="form-control" id="IsiP" name="IsiP" placeholder="Pertanyaan Anda?">
                    @error('IsiP')
                   <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>
                  </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
</div>
</div>

@endsection