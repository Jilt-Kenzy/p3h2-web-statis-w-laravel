
@extends('AdminLTE.Master')

@section('Content')

<a href="/Pertanyaan/Create" class="btn btn-primary">Tambah</a>
        <table class="table">
            <thead class="thead-light">
              <tr>
                <th scope="col">#</th>
                <th scope="col">Title</th>
                <th scope="col">Body</th>
                <th scope="col" style="display: inline">Actions</th>
              </tr>
            </thead>
            <tbody>
                @forelse ($Pertanyaan as $key=>$value)
                    <tr>
                        <td>{{$key + 1}}</th>
                        <td>{{$value->Judul}}</td>
                        <td>{{$value->Isi}}</td>
                        <td>
                            <a href="/Pertanyaan/{{$value->id}}" class="btn btn-info">Show</a>
                            <a href="/Pertanyaan/{{$value->id}}/Edit" class="btn btn-primary">Edit</a>
                            <form action="/Pertanyaan/{{$value->id}}" method="POST">
                                @csrf
                                @method('DELETE')
                                <input type="submit" class="btn btn-danger my-1" value="Delete">
                            </form>
                        </td>
                    </tr>
                @empty
                    <tr colspan="3">
                        <td>No data</td>
                    </tr>  
                @endforelse              
            </tbody>
        </table>


@endsection