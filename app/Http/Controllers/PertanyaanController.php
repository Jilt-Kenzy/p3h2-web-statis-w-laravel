<?php

namespace App\Http\Controllers;

use Facade\Ignition\QueryRecorder\Query;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class PertanyaanController extends Controller
{
    //
    public function indext(){
        $Pertanyaan = DB::table('pertanyaan') ->get();
        // dd($Pertanyaan);
        return view('Pertanyaan.IndextP',compact('Pertanyaan'));
    }

    public function create(){   //Fungsi untuk membuat pertanyaan
        return view('Pertanyaan.create');
    }
    public function store(Request $request){ //Penyimpanan 
        
           $data = $request->validate([
            'JudulP'=> 'required',
            'IsiP'=> 'required',            
        ]);
        // dd($request->all());
        $query = DB::table('pertanyaan')->insert([
            'Judul'=> $request['JudulP'],
            'Isi'=> $request['IsiP']
        ]);

        return redirect('Pertanyaan/Create');

    }
    public function show($id)
    {
        
        $pertanyaan = DB::table('pertanyaan')->where('id', $id)->first();
        return view('Pertanyaan.show', compact('pertanyaan')); 
        //Variabel disini pada arrray di difinisikan sebagai Compact("pertanyaan") yang mengacu pada nama table
        dd($pertanyaan);
        
    }

    public function edit($id)
    {
        $pertanyaan = DB::table('pertanyaan')->where('id', $id)->first();;
        return view('Pertanyaan.Edit', compact('pertanyaan'));
    }

    public function update($id, Request $request)
    {
        // dd($request->all())
        $query = DB::table('pertanyaan')
        ->where('id', $id)
        ->update([
            'Judul' => $request["JudulP"],
            'Isi' => $request["IsiP"]
        
        ]);
    return redirect('/Pertanyaan');

    }

    public function destroy($id)
    {
        $query = DB::table('pertanyaan')->where('id', $id)->delete();
        return redirect('/Pertanyaan');
    }

};


