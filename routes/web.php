<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//P3H2 - JAWABAN INTRO LARAVEL
Route::get('/Home', function () {
    return view('Home');
});

Route::get('/Register', 'AuthController@FormReg');
Route::get('/Welcome', 'AuthController@Halo');
Route::POST('/Welcome', 'AuthController@Halo');

//P3H3 - Theme Blade
Route::get('/Master', function () {
    return view('/AdminLTE/Master');
});
Route::get('/Item', function () {
    return view('Item.indext');
});
Route::get('/Create', function () {
    return view('Item.create');
});

//P3H3 - JAWABAN TUGAS
Route::get('/', function () {
    return view('P3H3.tableawal');
});
Route::get('/data-tables', function () {
    return view('P3H3.data-tables');
    
});


//P3H5 - CRUD LARAVEL
Route::get('/Pertanyaan', 'PertanyaanController@Indext'); //Menampilkan Pertanyaan
Route::get('/Pertanyaan/Create', 'PertanyaanController@create'); //Membuat Pertanyaan
Route::post('/Pertanyaan', 'PertanyaanController@store'); // Route untuk menyimpan pertanyaan
Route::get('/Pertanyaan/{Pertanyaan_id}','PertanyaanController@show'); // Route untuk menampilkan detail pertanyaan
Route::get('/Pertanyaan/{Pertanyaan_id}/Edit','PertanyaanController@edit');
Route::put('Pertanyaan/{Pertanyaan_id}', 'PertanyaanController@update');
Route::delete('Pertanyaan/{Pertanyaan_id}', 'PertanyaanController@destroy');
